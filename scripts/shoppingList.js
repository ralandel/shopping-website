$( function() {

    console.log("jQuery is correctly loaded")

    var ListElement = [];

    $("#addItemButton").click(function() {

        console.log("button clicked");

        AddItem();

    });

    $("#item").keypress(function(event) {
        if (event.which === 13) { // event 13 is key Enter press
          AddItem();
        }
    });

    $("#clearListButton").click(function() {

        console.log("Clear list button clicked");

        ClearList();

    });


    function AddItem() {

        var newItem = $("#item").val();

        var ElementNotinList = ListElement.includes(newItem);
    
        if (newItem.trim() !== "" &&  !ElementNotinList) { // check that no empty elements are added and not already added
            var newListItem = $("<li>").text(newItem); 
        
            var deleteButton = $("<button>").text("Delete");

            deleteButton.click(function() {

                if (confirm("Do you want to delete this element ?")) {
                    $(this).parent().remove();
                }
        
                console.log("element deleted" )
        
                $(this).parent().remove();

            });

            newListItem.append(" | prix : ");

            var Price = ' $' + (Math.random() * (99 - 0.99) + 0.99).toFixed(2).toString();

            newListItem.append(Price);

            newListItem.append("&nbsp;"); // Add a space between text and button

            newListItem.append(deleteButton);

    
            $("ul").append(newListItem);
    
            $("#item").val("");

            console.log("element created : " + newItem);

            ListElement += newItem;

        } else {
            
            if (newItem.trim() === "") {

                alert("Invalid empty element");

            } else {

                alert("Element already added");

            }
        }
    };

    function ClearList() {

        if (ListElement.length === 0) {
            alert("there is no element !")
        }
        if (confirm("Do you want to clear the list ?")) {
            $("ul").empty(); 

            ListElement = []; 

        }
    };
});